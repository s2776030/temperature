package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public int getCelcius(String celcius){
        return (Integer.parseInt(celcius) * 9/5) + 32;
    }
}
